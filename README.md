# Too Long; Don't Watch

### [Video Showcase](https://youtu.be/Tm9zbi_Y6Q0)
---

### The chrome extension works by using the OpenAI "text-davinci-002" ML engine in combination with a YouTube transcript API.
### These tools together allowed me to return a detailed summary of a YouTube video from just the url.

---

![alt text](/images/chrome_8Fixl3esUQ.png)

---

![alt text](/images/XH1pi5XSGp.png)

---
### The extension also works as a webpage
![alt text](/images/chrome_T9JKkJ18JR.png)
