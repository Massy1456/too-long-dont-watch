from youtube_transcript_api import YouTubeTranscriptApi
from rest_framework.response import Response
from rest_framework.decorators import api_view
import os
import openai

from dotenv import load_dotenv
load_dotenv()

openai.api_key = os.environ.get("OPENAI_API_KEY")
openai.organization = "org-0XxHgXnHtk5agJ65cogNJKE8"


def filterString(link):  # filter youtube string
    youtube_id = link.split("=")[-1]
    if len(youtube_id) > 11:
        youtube_id = youtube_id.split('&')[0]
    return youtube_id


def getTranscript(id):  # create string with entire transcript
    transcript_list = YouTubeTranscriptApi.get_transcript(id)
    full_transcript_text = ''
    for i in range(len(transcript_list)):
        full_transcript_text += transcript_list[i]['text'] + ' '
    return full_transcript_text


def trimTranscript(transcript):  # transcript too long, trim it
    if len(transcript) > 18000:
        return transcript[:18000]
    return transcript


def getSummary(transcript):  # send transcript to OpenAI engine
    return openai.Completion.create(
        model="text-davinci-002",
        prompt=f"Can you summarize this? '{transcript}'",
        temperature=0.6,
        max_tokens=100,
    )


@api_view(['POST'])
def ai_response(req):

    raw_link = req.data['text']  # get the raw link from user
    youtube_id = filterString(raw_link)
    transcript = getTranscript(youtube_id)
    transcript = trimTranscript(transcript)
    transcript_summary = getSummary(transcript)

    return Response(transcript_summary)
