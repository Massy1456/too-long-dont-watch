from django.urls import path
from . import views

urlpatterns = [
    path('openai/', views.ai_response),
]
